# 欢迎来到MindSpore Transformer

## GPT Language model下游任务

### 数据集
- 微调训练集：
  用于微调的数据集已经清理并且转换为mindspore格式，存储在wikitext_output/train文件夹中
- 微调后测试集：
  微调后用于测试的数据集已经清理并转换为mindspore格式，存储在wikitext_output/test文件夹中

### 下游任务微调
```shell
bash examples/finetune/run_language_model.sh
```
需要在脚本中设置load_pretrain_ckpt_path参数，这个参数为预训练的GPT权重的路径。

### 下游任务测试
```shell
bash examples/inference/predict_gpt_lm.sh 
```
需要在脚本中设置load_eval_ckpt_path参数，这个参数为下游任务微调后的模型权重的路径， 运行之后会输出测试的PPL与Loss